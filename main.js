const nSpendingThresold = 40000;
const nPhonePrice = 10000;
const nAccessoryPrice = 5000;
const nTextRate = 5;

let nBankBalance = 70000;
let combo = 0;
let nTotalphone = 0;
let nAccessoryTotal = 0;
let nAmount = 0;
let nTotalAmount = 0;
let nTaxAmount = 0;
let nBudget = nSpendingThresold;
let nCombo = nPhonePrice + nAccessoryPrice + getTax(nPhonePrice) + getTax(nAccessoryPrice);
let nPhoneTotal = nPhonePrice + getTax(nPhonePrice);

//check bank balance
while (nBankBalance > 0) {
    //check both are in Budget or not
    if (nCombo <= nBudget) {
        //for counting number of phone purchase
        nTotalphone++;
        //for counting number of accessories purchase
        nAccessoryTotal++;       
        //Reduce thresold according to purchase
        nBudget -= nCombo;
        //Reduce bank balance according to purchase
        nBankBalance -= nCombo;       
    }
    //Now purchase phone until balance run out
    else if (nPhoneTotal <= nBankBalance) {
        nTotalphone++;
        nBankBalance = nBankBalance - nPhoneTotal;
    }
    else {
        break;
    }
}

//function to calculate tax
function getTax(nAmount) {
    nTaxAmount = (nAmount * nTextRate) / 100;
    return nTaxAmount;
}
console.log("Phone Price: $"+nPhonePrice);
console.log("Accessoires Price: $"+nAccessoryPrice);
console.log("Tax Rate: "+nTextRate);
console.log("Total of each Phone and Accessoires: $"+nCombo.toFixed(2));
console.log("Total Phone Purchase: "+nTotalphone);
console.log("Total Accessories Purchase: "+nAccessoryTotal);
console.log("Remaining Bank Balance: $"+nBankBalance.toFixed(2));

document.getElementById("nphoneprice").innerHTML = nPhonePrice
document.getElementById("nAccessoryPrice").innerHTML = nAccessoryPrice
document.getElementById("nTextRate").innerHTML = nTextRate
document.getElementById("nTotalphone").innerHTML =  nTotalphone
document.getElementById("nAccessoryTotal").innerHTML = nAccessoryTotal
document.getElementById("nBankBalance").innerHTML = nBankBalance


